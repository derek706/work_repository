/* 位置區塊展開收合效果 */

$(".browsing_location button").click(function(){

	"use strict";

	$(".browsing_location").toggleClass("show");

});



/* 成功案例 */

$("#success-case-list").append("<button>+</button>");

$("#success-case-list button").click(function(){

	"use strict";

	$(this).parent().toggleClass("open");

	$(this).html("+");

  $("#success-case-list.open button").html("-");

});



/* 學生資料名單 */

$("#student-list").append("<button>+</button>");

$("#student-list button").click(function(){

	"use strict";

	$(this).parent().toggleClass("open");

	$(this).html("+");

  $("#student-list.open button").html("-");

});



/* 選單點擊時焦點屬性自動添加 */

$(window).on("load", function () {

	"use strict";

	var j_setting = {

		tag:'products',					//-- *命名空間 不可重複使用

      	main_body: '.in-left-menu>ul, .in-left-menu>.m_classLink>ul',//--目標主體第一層大框架

		item:'.in-left-menu>ul>li, .in-left-menu>.m_classLink>ul>li',	//-- *第一層目標物件群 EX: '.prolist ul>li'

		resetitem:'#menu li:eq(1)', //-- *重置物件 (通常放到上板選單)

		addclass:'active',			//-- *增加的class屬性

		default:0,					//-- *預設第幾個為選取

		action:{main:'menu1',menu_child_body:'ul',menu_child_list:'li'}			//-- 模式 類型  menu1收合式選單

	};



	function work(j_setting){

		sessionStorage[j_setting.tag] = (typeof(sessionStorage[j_setting.tag])!=="undefined" ? sessionStorage[j_setting.tag]:j_setting.default);

		$(j_setting.item).each(function(idx,obj){

			$(obj).attr('jtagid',idx);

		}).bind('click',function(event){

			sessionStorage[j_setting.tag] = $(event.target).attr('jtagid');

		});

		$(j_setting.resetitem).bind('click',function(){sessionStorage[j_setting.tag]=j_setting.default;});

		$(j_setting.main_body).find(j_setting.action.menu_child_list+'[jtagid="'+sessionStorage[j_setting.tag]+'"]').parentsUntil(j_setting.main_body).each(function(idx,obj){

			if ($(obj).attr('jtagid')!==null && $(obj).attr('jtagid')!==''){

				$(obj).addClass(j_setting.addclass);

			}

		});

		if (typeof(j_setting.action)!=="undefined") {menu(j_setting);}

	}



	var section_back = function (e){

		var check_child = false;

		sessionStorage[j_setting.tag] = ($(e.target).attr('jtagid') ? $(e.target).attr('jtagid'):$(e.target).parentsUntil(j_setting.action.menu_child_body).last().attr('jtagid'));



     var 	notwork = $(e.target).parentsUntil(j_setting.main_body).last();

		$(j_setting.item).not(notwork).find(j_setting.action.menu_child_body).filter(':visible').slideUp();



		$(e.target).parentsUntil(j_setting.action.menu_child_body).each(function(idx,obj){

			if ($(obj).find(j_setting.action.menu_child_body).filter(':visible').length<=0 && $(obj).find(j_setting.action.menu_child_body).length>0 && $(obj).find(j_setting.action.menu_child_body)[0].tagName.toLowerCase()===j_setting.action.menu_child_body.toLowerCase()) {

				check_child = false;

				$(obj).find('>'+j_setting.action.menu_child_body).stop().slideDown().find(j_setting.action.menu_child_body).hide();

			}else if ($(obj).find(j_setting.action.menu_child_body).length<=0){

				check_child = true;

			}

		});





		return check_child;

	};

	function menu(j_setting){

		$(j_setting.item).each(function(idx,obj){

			$(obj).on('click',section_back);

		});



		switch (j_setting.action.main){

			case "menu1":

				$(j_setting.item).find(j_setting.action.menu_child_body).find(j_setting.action.menu_child_list).each(function (idx,obj){

					$(obj).attr('jtagid',$(j_setting.item).length+idx);

				}).on('click',section_back);

			break;

			default:

			break;

		}



		$(j_setting.item).find(j_setting.action.menu_child_body).hide();



		if (sessionStorage[j_setting.tag]){

			if ($(j_setting.main_body).find(j_setting.action.menu_child_list+'[jtagid="'+sessionStorage[j_setting.tag]+'"]').parentsUntil(j_setting.main_body).length>0){

				$(j_setting.main_body).find(j_setting.action.menu_child_list+'[jtagid="'+sessionStorage[j_setting.tag]+'"]').parentsUntil(j_setting.main_body).each(function(idx,obj){

					$(obj).addClass(j_setting.addclass);

					if ($(obj)[0].tagName===j_setting.action.menu_child_body.toUpperCase()) { $(obj).show(); }

				});

			}

			$('[jtagid="'+sessionStorage[j_setting.tag]+'"]').addClass(j_setting.addclass).find('>'+j_setting.action.menu_child_body).show();

		}



	}



	work(j_setting);

});





/* 手风琴收合效果 */

$(function(){

	// 幫 div.qa_title 加上 hover 及 click 事件

	// 同時把兄弟元素 div.qa_content 隱藏起來

	$('#qaContent ul.accordionPart li div.qa_title').hover(function(){

		$(this).addClass('qa_title_on');

	}, function(){

		$(this).removeClass('qa_title_on');

	}).click(function(){

		// 當點到标题時，若答案是隱藏時則顯示它；反之則隱藏

		$(this).next('div.qa_content').slideToggle();

	}).siblings('div.qa_content').hide();

	// 全部展開

	$('#qaContent .qa_showall').click(function(){

		$('#qaContent ul.accordionPart li div.qa_content').slideDown();

		return false;

	});

	// 全部隱藏

	$('#qaContent .qa_hideall').click(function(){

		$('#qaContent ul.accordionPart li div.qa_content').slideUp();

		return false;

	});

	// 关闭

	$('#qaContent .close_qa').click(function(){

		$(this).parents('.qa_content').prev().click();

		return false;

	});

});





/* 团队 */

$(".school-photo-2").append("<button>+</button>");

$(".school-photo-2 button").click(function(){

	"use strict";

	$(this).parent().toggleClass("open");

	$(this).html("+");

  $(".school-photo-2.open button").html("-");

});



/* 新資訊 */

if($(".news.news1.hide-more>ul.news-list>li:nth-child(11)").length){

	$(".news.news1.hide-more").append("<button>more</button>");

	$(".news.news1.hide-more button").onclick(function(){

		"use strick";

		$(this).parent().toggleClass("open");

		$(this).html("more");

		$(".news.news1.hide-more.open button").html("close");

	});

}
// 如果li長度大於11啟動，增加一個button，在按下時，在這個上面增加open，並改變內容為close





/* 问答FAQ */

if($(".faq.hide-more ul.accordionPart li:nth-child(11)").length){

	$(".faq.hide-more").append("<button>more</button>");

	$(".faq.hide-more button").click(function(){

		"use strick";

		$(this).parent().toggleClass("open");

		$(this).html("more");

		$(".faq.hide-more.open button").html("close");

	});

}

/* FAQ拓展按鈕通用 */

$("#extend_active").append("<button>+</button>");

$("#extend_active button").click(function(){

	"use strict";

	$(this).parent().toggleClass("open");

	$(this).html("+");

  $("#extend_active.open button").html("-");

});



/* 优惠课程 */

if($(".news.news2.hide-more>ul.news-list>li:nth-child(11)").length){

	$(".news.news2.hide-more").append("<button>more</button>");

	$(".news.news2.hide-more button").click(function(){

		"use strick";

		$(this).parent().toggleClass("open");

		$(this).html("more");

		$(".news.news2.hide-more.open button").html("close");

	});

}



